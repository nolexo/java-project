/*
        -- zgłoski
        -----------------------------------------------------------------------------
        - napisz program, który zliczy wszystkie występujące w podanym słowie samogłoski i spółgłoski
        - przykładowe wyjście po uruchomieniu programu
        wyraz 'slowo' składa się z
        samogłoski: 2
        spółgłoski: 3
        - bonus dla ambitnych - wypisz wszystkie unikalne litery, które wystąpiły w słowie w kolejności alfabetycznej, np.
        l,o,s,w
        */


import java.util.*;
import java.util.List;


public class Zgloski {
    public static void main(String[] args){
        while (true){

            int consonants = 0 , vowel =0;
            char [] unique ;
            Scanner get = new Scanner(System.in);
            System.out.print("poda jakieś słowo\naby zakonczyc wpisz exit\n");
            String text = get.nextLine();

            if(text.equals("exit")) {
                break;
            }
            for (int i =0; i<text.length(); i++){


                if(check_ascii(text.charAt(i))){

                    consonants += 1;
                }

            }
            vowel = text.length() - consonants;
            System.out.print("Samogłoski: " + vowel + "\nSpółgłoski: " + consonants + "\n");

            System.out.print("Słowo z unikalnych liter: " + unique(text) + "\n");

            consonants = 0;
            vowel = 0;
        }


    }
    public static boolean check_ascii(char number){


        Integer [] consonants = {97, 101 , 105 , 111, 117, 121};
        int asci = number ;
        for (int i =0; i < consonants.length; i++) {
            if (consonants[i].equals(asci))
                return true;
        }
        return false;
    }
    public static String unique(String oldList){
        

        List <Character> myList = new ArrayList<Character>();
        String newList = "" + oldList.charAt(0);
        myList.add(oldList.charAt(0));

        for(int i = 1; i < oldList.length(); i++){

            for(int j=0; j< newList.length(); j++) {

                if (newList.charAt(j) == oldList.charAt(i)) {
                    break;
                }
                else if (newList.length() == j + 1) {
                    newList = newList + oldList.charAt(i);
                    myList.add(oldList.charAt(i));
                }
            }

        }

        Collections.sort(myList);
        System.out.print("unikalne litery ułożone alfabetycznie: " + myList + "\n");
        return  newList;
    }
}