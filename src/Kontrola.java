/*

-----------------------------------------------------------------------------
-- kontrola rodzicielska
-----------------------------------------------------------------------------
- napisz program, który
- poprosi użytkownika o podanie swojego wieku
- jeśli wiek będzie mniejszy niż 18 lat to wyświetl komunikat, że alko dostępne jest dopiero od 18roku życia
- jeśli wiek będzie >= 18 lat, podziękuj za dokonanie zakupów

 ===============================================================================================================
        */




import java.util.*;



public class Kontrola {
    public static void main(String[] args){
        int age;
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj swój wiek do autoryzacji:");
        String howOld = scanner.nextLine();
        
        while(true){
        
            try{
            
                age = Integer.valueOf(howOld);
                break;
            }
            catch (Exception e){
            
                System.out.print("podaj swój wiek do autoryzacji:");
                howOld = scanner.nextLine();

            }
        }
        if(age < 18){
			
            System.out.print("alkohol dostępny od 18 lat\n");
			
		}
        else{
			
            System.out.print("dziękujemy za dokonanie zakupów");
		}


    }


}