/*
   -----------------------------------------------------------------------------
        -- funkcje
        -----------------------------------------------------------------------------
        - napisz funkcje, która w wyniku swojego działania zwróci liste postaci:
        { random, random, random,
        random, random, random,
        random, random, random }

        gdzie "random" ma być wartością randomowo wybraną z listy [ 'x','y' ]
        przykład:
        import random
        xy = [ 'x', 'y' ]
        a = { random(xy) }

        przykładowa deklaracja funkcji:
        def gen_random_list():
        xy = [ 'x', 'y' ]
        lista = ?????
        ???????????????
        return list

        powstałą funkcję wywołaj w kodzie programu i print'nij zwrócona liste

        - napisz kolejną funkcję, która wypiszę listę w ładnej dla oka postaci, np.
        + - - - +
        | 1 2 3 |
        | 4 5 6 |
        | 7 8 9 |
        + - - - +
        ========================================================================================================
        */




        import java.util.*;
        import java.util.List;


public class Funkcje {
    public static void main(String[] args){

        List <Character> myList = new ArrayList<Character>();
        myList = sprawdz_asci();
        System.out.print(myList + "\n");
        draw(myList);

    }
    public static List sprawdz_asci(){
        List <Character> xy = new ArrayList<Character>();
        xy.add('x');
        xy.add('y');
        List <Character> myList  = new ArrayList<Character>();
        Random random = new Random();
        for(int i = 0; i < 9; i++){
            myList.add(xy.get(random.nextInt(2)));
		}
        
        return myList;
    }
    public static void draw(List <Character> myList){
        System.out.print( "+-----+\n" +
                "|" + myList.get(0) + " " + myList.get(1) + " " + myList.get(2) + "|\n"+
                "|" + myList.get(3) + " " + myList.get(4) + " " + myList.get(5) + "|\n"+
                "|" + myList.get(6) + " " + myList.get(7) + " " + myList.get(8) + "|\n"+
                "+-----+");
    }

}