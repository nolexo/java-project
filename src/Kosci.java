/*

-----------------------------------------------------------------------------
-- gra w kości
-----------------------------------------------------------------------------
- napisz prostą grę w kości.
Zasady:
  - gra kończy się po 3 rundach
  - jedna runda składa się z dwóch ruchów, jeden ruch to rzut kośćmi przez gracza, drugi ruch to rzut kośćmi przez komputer
  - każdy z graczy rzuca dwiema kostkami sześciościennymi (można wyrzycić od 1 do 6)
  - po każdym ruchu gracza należy zsumować wynik wylosowanych oczek z poprzednimi, tak, żeby na koniec gry (po 3 rundach) dostać sumę wszystkich wyrzuconych oczek
  - wygrywa ten, kto na koniec trzeciej rundy będzie miał więcej oczek
  - możliwy jest remis
  +(bonus dla ambitnych) - w przypadku remisu należy rozpocząć grę od nowa (ponownie rozegrać 3 rundy)

  ===================================================================================================
        */

import java.util.*;


public class Kosci {
    public static void main(String[] args){
        int rounds  = 0;
        String name = "";
        Scanner scanner = new Scanner(System.in);

        while (name.length() < 3 ){
            System.out.print("Podaj swoją nazwę (min 3 litery bez spacji)\n");
            name=scanner.nextLine();
			
            if (name.contains(" ")){
                name = "";
            }
        }

        Player whoPlaying;
        Player player = new Player(name);
        Player com    = new Player( "Komputer");
        whoPlaying = player;


        while(true){

            if(whoPlaying.name == name){


                System.out.print("Twój ruch wciśnij enter aby rzucić koścmi\n");
                scanner.nextLine();
            }
            throw_dice(whoPlaying);
            System.out.print("Punkty " + player.name +" " + player.points+ " Punkty Komputer: " + com.points + "\n\n");
			
            if(whoPlaying.name == player.name){

                whoPlaying = com;
            }
            else{

                whoPlaying = player;
            }
            rounds++;
            if(rounds/3 == 2){

                if(player.points > com.points){

                    System.out.print("Gratulacje wygrałeś!");
                    break;
                }
                else if(player.points<com.points){

                    System.out.print("Wygrał komputer!");
                    break;
                }
                else{

                    System.out.print("Remis Dogrywka!\n");
                    player.points = 0;
                    com.points    = 0;
                    rounds        = 0;
                }
            }
        }

    }
    public static void throw_dice(Player whos){

        int count = new Random().nextInt(6)+1 + new Random().nextInt(6)+1;
        whos.points += count;
        System.out.print(whos.name + " wyrzucił " + count + "\n");

    }



}